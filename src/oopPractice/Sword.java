package oopPractice;

public class Sword {

    // 1. constructor
    //default constructor
    public Sword(){

    }
    public Sword(String text){
        System.out.println(text);
    }
    //2. Instance variables

    // static -> belonging to the class, What does every sword has?
    public static final boolean hasHandle = true;
    public static boolean isSharp;


    //non static -> belongs to the object, What is changing sword by sword?
    public double weight;
    public double length;
    public String name;
    public String material;

    // 3. methods -> what does a sword do
    public static void slash(){
        System.out.println("Sword is slashing");
    }

    //non-static ones -> belongs to the object, what does every sword do differently?
    // weight * length = 2 pounds * 10 = 20 points
    // weight * length = 10 pounds * 15 = 150 points

    public void harms(){
        System.out.println("Sword damages = " + (weight * length) + " points.");
    }

    //running only ONCE before EVERYTHING
    static{
        System.out.println("Starting the sword making furnace");

    }
    //running everytime when you create object before everything after static code block
    {

    }


}
