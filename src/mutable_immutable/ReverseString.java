package mutable_immutable;

public class ReverseString {
    public static void main(String[] args) {

        System.out.println(reverseString("Hello")); // olleH
        System.out.println(isPalindrome("civic")); // true
        System.out.println(isPalindrome("madam"));// false


    }

    /*
    write a method that takes a string and returns it back reversed

     */

    public static String reverseString(String str) {
       String result = "";
       for (int i = str.length()-1; i >= 0; i--){
           result += str.charAt(i);
       }
       return result;
       /*
       Hello

       Result
       o
       ol
       oll
       olle
       olleH
       These are 5 different strings in the String Pool
        */
    }
    public static String reverseAString(String str){
        StringBuilder result = new StringBuilder(str); //Hello
        result.reverse();
        return result.toString();

        //return new StringBuilder(str).reverse().toString();
        //take string, reverse it, take it back to String

    }
    //checking if it is palindrome using reverse and equals methods
    public static boolean isPalindrome(String str){
        return new StringBuilder(str).reverse().toString().toLowerCase().equals(str);
        //Not taking as much memory in the pool

    }
}
