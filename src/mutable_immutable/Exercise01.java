package mutable_immutable;

public class Exercise01 {
    public static void main(String[] args) {

        System.out.println(insertMiddle("a"));
        System.out.println(insertMiddle("aa"));
        System.out.println(insertMiddle("bbb"));

    }

    /*
    create a method that takes a String and returns a String
    if the given string has even length and length is at least 2,
    then insert * in the middle of the String

    aa -> a*a
    if the given string has odd length() and length is atleast 3,
    then insert * before and after the middle character
    aaa -> a*a*a
    olena -> ol*e*na

    if the String is empty or having length of 1, then return String itself
     */

    public static String insertMiddle(String str) {

        if (str.length() <= 1) return str;

        if (str.length() % 2 == 0) { // aa bbbb
            return new StringBuilder(str).insert(str.length() / 2, "*").toString();
        } else { // b*b*b cc*c*cc 3/2 -> 5/2 length()/2  4/2 -> 2 + 1   6/2 -> 3+1 length() /2 + 2
            return new StringBuilder(str).insert(str.length() / 2, '*').insert(str.length() / 2 + 2, "*").toString();

        }

    }

    }

