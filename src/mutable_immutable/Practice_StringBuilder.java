package mutable_immutable;

public class Practice_StringBuilder {
    public static void main(String[] args) {
        StringBuilder city = new StringBuilder("Chicago");

        System.out.println(city); // if no args them prints empty

        // city = "Miami"; not possible
        city = new StringBuilder("Miami");

        String name = "Alex";
        name += "ander";

        System.out.println(name); // Alexander
        //this creates 3 strings stay in the String Pool: Alex, ander, and Alexander with name pointing to Alexander

        city.append("xxx");
        System.out.println(city); //Miamixxx
        //append is for StringBuilder/ StringBuffer objects and that will just update from Chicago to Miami
    }
}
