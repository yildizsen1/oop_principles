package oop_principles.polymorphism;

public class StaticVsDynamicPolymorphism {
    public static void main(String[] args) {

        Animal animal = new Animal();
        animal.eat(); // static polymorphism // Animal eats

        Animal dog = new Dog(); // dynamic polymorphism - upcasted dog object
        dog.sleep(); // Dog sleeps (referenced by Animal)
        /*when it executes the compiler thinks the animal method will be executed
        but RunTime finds out that the dog method will be executed */

        /*
        Dynamic Polymorphism:
        method overriding

        Static Polymorphism:
        method overloading
         */
    }
}
