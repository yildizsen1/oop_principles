package oop_principles.polymorphism;

public class TestPolymorphism {
    public static void main(String[] args) {

        Animal a1 = new Animal();

        //they are called by the object because they are not static
        a1.eat(); // Animal eats
        a1.eat("Meat"); //Animal eats meat
        a1.sleep(); // Animal sleeps

        Cat c1 = new Cat();
        c1.eat(); //Animal eats
        c1.eat("Fish"); //Animal eats Fish
        c1.sleep(); //Animal sleeps
        c1.meow(); //Cat meows (new method in child class)

        Dog d1 = new Dog();
        d1.eat(); // Dog eats (overridden methods in child class)
        d1.eat("Bone"); // Dog eats Bone
        d1.sleep(); // Dog sleeps (overridden methods in child class)
        d1.bark(); // Dog barks

        /*Up-casting (child in shape of parent)
        parent reference = child reference
         */

        System.out.println("\n--------upcasting - implicitly--------\n");
        Animal a2 = new Cat(); // will not include meow method
        Animal a3 = new Dog(); // will not include bark method

        a3.sleep(); //Dog sleeps (after running it looks to the object)


        System.out.println("\n--------down-casting - explicitly--------\n");
        //Cat cat = new Animal(); ->this must be casted
        //Cat cat = (Cat) new Animal(); // object is Animal -> cant find moew() -> bad example
                                        //ClassCastException
        /*
        In order to down cast, you must upcast first
         */
        Animal myDog = new Dog(); // upcasting
       // Dog newDog = myDog; // must cast
        Dog newDog = (Dog) myDog; // one reference from parent, once reference from self -> object is Dog()

        newDog.bark(); // Dog barks


        //Examples:
      /*  Animal a10 = new Cat();
        Dog d10 = (Dog) a10; //ClassCastException -> a10 = new Cat means Dog d10 = new Cat(); cant make a cat a dog

       */

        Animal a11 = new Cat(); // three references a11/ a12 c11
        Animal a12 = a11;
        Cat c11 = new Cat(); // this object will be garbage collected
        c11 = (Cat) a11; // this refers to first new Cat();

    }

}
