package oop_principles.polymorphism;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class PolymorphicCollection {
    public static void main(String[] args) {

        System.out.println("\n--------------Animal object------------\n");
        Animal animal1 = new Animal();
        animal1.eat();
        animal1.eat("Food");
        animal1.sleep();

        System.out.println("\n--------------Cat object------------\n");
        Cat cat = new Cat();
        cat.eat(); // Animal eats
        cat.eat("Fish"); // Animal eats Fish
        cat.sleep(); // Animal sleeps
        cat.meow(); // cat meows
        //first 3 are inherited from the Animal object
        //meow() is a new method only in the Cat class

        System.out.println("\n--------------Dog object------------\n");
        Dog dog = new Dog();
        dog.eat(); // Dog eats
        dog.eat("meat"); // Dog eats meat
        dog.sleep(); // Dog sleeps
        dog.bark(); // Dog barks
        // We see dog because we overrode those methods

        System.out.println("\n--------------Cat object in the shape of Animal-----------\n");
        Animal animal2 = new Cat(); // cat in the shape of animal -> you lose meow()
        animal2.sleep(); //Animal sleeps
        animal2.eat(); //Animal eats
        animal2.eat("Fish");

        System.out.println("\n--------------Dog object in the shape of Animal-----------\n");
        Animal animal3 = new Animal(); // dog in the shape of animal
        animal3.eat(); // Animal eats
        animal3.eat("Bone");
        animal3.sleep();

        System.out.println("\n--------------Store all above objects in an array-----------\n");
        Animal[] animals = {animal1, animal2, animal3, cat, dog};
        for(Animal a : animals){
            System.out.println(a.getClass().getSimpleName());
            a.eat();
            a.eat("str");
            a.sleep();
        }
        /*
        Animal
        Cat
        Animal
        Cat
        Dog
         */
        ArrayList<Animal> animalList = new ArrayList<>(Arrays.asList(animals));
        new ArrayList<>(Arrays.asList(animals)).forEach(Animal::eat);







    }
}
