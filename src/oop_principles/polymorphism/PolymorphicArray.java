package oop_principles.polymorphism;

import java.util.ArrayList;
import java.util.Scanner;

public class PolymorphicArray {
    public static void main(String[] args) {

        int[] numbers = {1, 2, 3}; // simplest form of array / Integer.parseInt("123")} to convert String to digit
        String[] names = {"Alex", "John"};

        /*
        Alex, John
        'X', 'b'
        12
        4.5
        Scanner object

         */

        //object is the parent of everything
        Object[] objects = {1, 2, 4.5, 10.33, 'X', "Alex", "John", new Scanner(System.in), new ArrayList<>(), new Dog(), new Animal()};

        double d = (double)objects[2] + 5; //9.5
        System.out.println(d);

       // Dog dog = (Dog) objects[8]; // ClassCastException
       // dog.bark();

        Dog dog = (Dog) objects[9];
        dog.bark();
        dog.sleep();

        ArrayList<String> names1 = (ArrayList<String>) objects[8];
        System.out.println(names1.size()); //0


    }
}
