package oop_principles.inheritance;

public class Tester extends Person {

    //default constructor
    public Tester() {

    }

    //3 args constructor
    public Tester(String fName, int age, String DOB) {
        super(fName, age, DOB);
    }

    //4 arg constructor
    public Tester(String fName, int age, String DOB, String SSN) {
        super(fName, age, DOB, SSN);
    }

    //5 arg constructor
    public Tester(String fName, int age, String DOB, String SSN, boolean isAutomationTester) {
        super(fName, age, DOB, SSN);
        this.isAutomationTester = isAutomationTester;
    }

    public boolean isAutomationTester;

    //2 args constructor
    public Tester(String fName, int age) {
        super(fName, age);

       /* super.fName = fName;
          super.age = age;
        */

    }

    public void code() {
        System.out.println("Tester codes");
    }

    //overload code method
    public void code(String language) {
        System.out.println("Tester code in " + language + " language");
    }

    @Override
    public void eat() {
        System.out.println("Tester eats");
    }

    @Override //optional
    public void sleep() {
        System.out.println("Tester sleeps");
    }

    @Override
    public String toString() {
        return "Tester{" +
                "fName='" + fName + '\'' +
                ", age=" + age +
                ", DOB='" + DOB + '\'' +
                ", SSN='" + getSSN() + '\'' +
                ", isAutomationTester=" + isAutomationTester +
                '}';
    }
}