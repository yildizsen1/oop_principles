package oop_principles.inheritance;

public class Person extends Object {

    //default constructor is here
    public Person(){}

    //2-args contructor with fname and age

    public Person(String fName, int age){
        this.fName = fName;
        this.age = age;
    }

    //3 args with f name, age, and dob
    public Person(String fName, int age, String DOB){
        this(fName, age);
        this.DOB = DOB;
    }

    //4 args contructor with all fields
    public Person(String fName, int age, String DOB, String SSN){
        this(fName, age, DOB);
        this.SSN = SSN;
    }

    //instance variables
    public String fName;
    protected int age;
    String DOB;
    private String SSN;

    public String getSSN() {
        return SSN;
    }

    public void setSSN(String SSN) {
        this.SSN = SSN;
    }

    public void eat(){
        System.out.println("Person eats");
    }

    // overload eat method with a String arguments
    public void eat(String str){
        System.out.println("Person eats " + str);
    }
    public void sleep(){
        System.out.println("Person sleeps");
    }
    public void sleep(int hours){
        System.out.println("Person sleeps " + hours + " hours");
    }

    //Override to String() Method

    @Override
    public String toString() {
        return "Person{" +
                "fName='" + fName + '\'' +
                ", age=" + age +
                ", DOB='" + DOB + '\'' +
                ", SSN='" + SSN + '\'' +
                '}';
    }
}
