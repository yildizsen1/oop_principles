package oop_principles.inheritance;

import java.util.Arrays;

public class TesterClub {
    public static void main(String[] args) {
        FrontendTester ft1 = new FrontendTester("Othman", 23, "1993", "000", true);
        BackendTester bt1 = new BackendTester("John", 45, "1982", "111", false);
        FrontendTester ft2 = new FrontendTester("Yildiz", 29, "1993", "222", false);
        BackendTester bt2 = new BackendTester("Pinar", 28, "1994", "333", false);

        Tester[] testers = {ft1, ft2, bt1, bt2}; //polymorphic array

        /*
        Manual testers = 3
        Automations testers = 1
        Average age = 33
         */

        int countManual = 0, countAutomation = 0, sumAge = 0;

        for (Tester tester : testers) {
            if(tester.isAutomationTester) countAutomation++;
            else countManual++;

            sumAge += tester.age;

        }
        System.out.println("Manual testers = " + countManual); // 3
        System.out.println("Automation testers = " + countAutomation); // 1
        System.out.println("Average age = " + sumAge / testers.length); // 33

        //counting with stream
        System.out.println(Arrays.stream(testers).filter(x -> x.isAutomationTester));






    }
}
