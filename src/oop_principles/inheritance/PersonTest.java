package oop_principles.inheritance;

public class PersonTest {
    public static void main(String[] args) {
        Person p1 = new Person();
        Tester t1 = new Tester();

        /*Person p2 = new Person("Alex", 25);
        Person p3 = new Person("John", 35, "10/10/2000");
        Person p4 = new Person("John", 35, "10/10/2000", "000-00-0000");

        Tester t2 = new Tester("Filiz", 15);
         */
         p1.eat(); //Person eats
         t1.eat(); //Person eats // must override

         p1.sleep(); //Person sleeps
         t1.sleep(); //Person sleeps // must override

        p1.sleep(10);
        t1.sleep(10);

        System.out.println(p1);
        System.out.println(t1);




    }
}
