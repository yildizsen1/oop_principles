package oop_principles.abstraction;

public class TestAbstraction {
    public static void main(String[] args) {
  //      Phone.call()- cannot be executed because there is no body in abstract method
        /*
        Phone phone = new Phone();
        phone.call(); - same thing, it still is being invoked using the method/ cannot be instantiated
         */

        Samsung s1 = new Samsung();
        s1.call();
        s1.text();

        iPhone i1 = new iPhone();
        i1.call();
        i1.text();

        Samsung s2 = new Samsung(64, "Galaxy s20", "Black", 700.0);
        iPhone i2 = new iPhone(64, "iPhone Pro Max", "White", 800.0);

        System.out.println(s2);
        System.out.println(i2);

        s2.takesPhoto();
        s2.recordVideo();
        i2.takesPhoto();
        i2.recordVideo();

        Nokia n1 = new Nokia();

    }
}
