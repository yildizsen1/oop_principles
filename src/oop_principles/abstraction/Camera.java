package oop_principles.abstraction;

public interface Camera {

    // all interface classes are public and abstract by default
    // these are additional features, it is optional to include interfaces

    void takesPhoto();
    void recordVideo();
}
