package oop_principles.encapsulation;

public class Test {
    public static void main(String[] args) {
        AccountHolder accountHolder = new AccountHolder();

        //accountHolder.firstName = "John";

        accountHolder.setFirstName("John", "5555");
        System.out.println(accountHolder.getFirstName("1234")); //null

        accountHolder.lastName = "Doe";
        accountHolder.address = "Chicago";
        accountHolder.phoneNumber = "213-000-0000";

        accountHolder.setSSN("000-00-0000");

        //System.out.println(accountHolder.firstName); // John
        System.out.println(accountHolder.getSSN()); // null
        System.out.println(accountHolder);

        accountHolder.setBalance(150.00);
        System.out.println(accountHolder.getBalance());

    }
}
