package oop_principles.encapsulation;

public class AccountHolder {

    private String firstName;
    String lastName;
    String address;
    String phoneNumber;
    private String SSN;
    private double balance;


    public String getFirstName(String passcode){
        if(passcode.equals("1234")) return firstName;
        else return null;
    }
    public void setFirstName(String firstName, String passcode){
        if(passcode.equals("1234")) this.firstName = firstName;
        else this.firstName = null;
    }
    public String getSSN(){ //retrieve private SSN via public method
        return this.SSN;
    }

    public void setSSN(String SSN){ //constructor, whatever you give to me I will set it in the object
        this.SSN = SSN;
    }

    //Create a double balance instance variable
    //encapsulate it
    public double getBalance(){
        return this.balance;
    }
    public void setBalance(double balance){
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "AccountHolder{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", SSN='" + SSN + '\'' +
                '}';
    }
}
