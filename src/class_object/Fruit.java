package class_object;

public class Fruit {

    public static boolean hasColor;
    public boolean isSweet;
    public String name;
    public String shape;

    // Instance block is executed for each object we created

    {
        //if(this.name.equals("Watermelon") isSweet = true;
        //more control/ dynamic
        //isSweet = true;
        System.out.println("This is an instance block");
    }
    //static block is executed only once and before everything else in the same class
    static{
        //connect to file here and read data
        hasColor = true;
        System.out.println("This is a static block");
    }
}
