package class_object;

public class Apple {

    //default constructor is provided by Java to create zero arg objects
        //because Apple() is not taking any arguments
        //but it can take arguments
        //same name as the class name - method names are meaningful
    public Apple(){
        //Each time you created a new object, this block will be executed
        //System.out.println("This is the default constructor");
    }

    //Overloading constructor with 3 args

    public Apple(String color, double price, String taste) {
        this.color = color;
        this.price = price;
        this.taste = taste;
    }

   //right click/ generate/ select constructor/ click all args/ click OK

    //instance variables - fields - states - attributes
    public String color; // null
    public double price; // 0.0
    public String taste; // null

   /* public String toString(){
        return "Apple{color: " + color + "," +
                "price: " + price + "," +
                "taste: " + taste + "}";

    */

    @Override
    public String toString() {
        return "Apple{" +
                "color='" + color + '\'' +
                ", price=" + price +
                ", taste='" + taste + '\'' +
                '}';
    }
}

