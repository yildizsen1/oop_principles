package class_object;

public class Car {

    //default constructor
    public Car(){

    }

    //Create 5-args overloaded constructor
    // keyword this is used to eliminate the confusion between class attributes and parameters with the same name
    public Car(String color, String make, String model, int year, double price) {
        this.color = color; //creating the class attribute within the constructor
        this.make = make;
        this.model = model;
        this.year = year;
        this.price = price;
    }

    //Instance variables
    public String color;
    public String make;
    public String model;
    public int year;
    public double price;

    //Override toString() method
    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", price=" + price +
                '}';
    }
}
