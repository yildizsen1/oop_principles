package class_object;

import static class_object.Student.numberOfStudents;

public class Animal {

    //Create contructor that takes 2 args of name and age


    //object arguments must match the constructor (if its 2 args - object must take 2 args)
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {

        String result = "name='" + name + '\'' +
                ", age=" + age + '\'';
        if (isCarnivore) result += ", is Carnivore=" + isCarnivore;
        if (isHerbivore) result += ", is Herbivore=" + isHerbivore;
        if (isOmnivore) result += ", is Omnivore=" + isOmnivore;
        result += "}";
        return result;
    }

    //Execute the code below whenever an Animal object is garbage collected
    //You will override finalize() method if you would like to execute a block of code right after your object is destroyed
    @Override
    protected void finalize() throws Throwable {
        System.out.println(getClass().getName() + this.name + " object is destroyed");
    }

    public Animal(String name, int age, boolean isCarnivore, boolean isHerbivore, boolean isOmnivore){
       // this.(name, age);
        this.name = name;
        this.age = age;
        this.isCarnivore= isCarnivore;
        this.isHerbivore = isHerbivore;
        this.isOmnivore = isOmnivore;


    }

    // instance variables
    public String name;
    public int age;
    public boolean isCarnivore;
    public boolean isHerbivore;
    public boolean isOmnivore;
    public static final boolean isExtinct = true;
/*
    {
        isCarnivore = true; //instance variable initilization
    }

    static{
        //initilize the static instance variables
        numberOfStudents = 0;
    }

 */

    public Animal() { // default constructor

    }


    public static void main(String[] args) {
        Animal a1 = new Animal(); // default constructor
        Animal a2 = new Animal("Dog", 3); //custom constructor - object arguments must match the constructor
        Animal a3 = new Animal("Cow", 5); //custom constructor
        Animal a4 = new Animal("Cat", 1); //custom constructor
        Animal a5 = new Animal("Tiger", 10, true, false, false);
        Animal a6 = new Animal("Cow", 10, false, true, false);
        Animal a7 = new Animal("Cat", 10, false, false, true);

        System.out.println("\n-------Printing the objects-------\n");
        System.out.println(a5);
        System.out.println(a6);
        System.out.println(a7);

        System.out.println(a2.name); //Dog
        System.out.println(a2.age); // 3


        System.out.println("\n------static instance variables-------\n");
        System.out.println(Animal.isExtinct); // true

        // Animal.isExtinct = false; cannot work because final



    }
}


